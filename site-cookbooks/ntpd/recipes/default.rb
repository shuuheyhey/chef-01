#
# Cookbook Name:: ntpd
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package "ntp" do
  action :install
end

template "/etc/ntp.conf" do
  source "ntp.conf.erb"
  owner "root"
  group "root"
  mode 0644
end

service "ntpd" do
  action [:enable, :start]
  supports :status => true, :restart => true, :reload => true
  subscribes :restart, resources(:template => "/etc/ntp.conf")
end
