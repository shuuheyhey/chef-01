name             'ntpd'
maintainer       'Funaffect'
maintainer_email 'm.oobayashi@funaffect.jp'
license          'All rights reserved'
description      'Installs/Configures ntpd'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
