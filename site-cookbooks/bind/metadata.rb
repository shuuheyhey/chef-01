name             'bind'
maintainer       'Funaffect'
maintainer_email 'm.oobayashi@funaffect.jp'
license          'All rights reserved'
description      'Installs/Configures bind'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
