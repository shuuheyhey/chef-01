%w{bind bind-libs bind-utils bind-chroot}.each do |pkg|
  package pkg do
    action :install
  end
end

link "/var/log/named" do
  to "/var/named/chroot/var/log"
  only_if "test ! -L /var/log/named"
end

template "/etc/logrotate.d/named" do
  source "logrotate.named.erb"
  owner "root"
  group "named"
  mode 0640
  notifies :restart, 'service[named]'
end