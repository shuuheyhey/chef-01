#
# Cookbook Name:: bind
# Recipe:: master
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'bind::package'

%w{empty localhost loopback}.each do |cfg|
  cookbook_file "/var/named/chroot/var/named/named." + cfg do
    source "named." + cfg
    owner "root"
    group "named"
    mode 0640
    action :create
  end
end

execute "create named.ca" do
  command "/usr/bin/dig @A.ROOT-SERVERS.NET. > /var/named/chroot/var/named/named.ca"
  action :run
  not_if { File.exists?("/var/named/chroot/var/named/named.ca") }
end

%w{rfc1918 rfc1912}.each do |rfc|
  cookbook_file "/var/named/chroot/etc/named." + rfc + ".zones" do
    source "named." + rfc + ".zones"
    owner "root"
    group "named"
    mode 0640
    action :create
  end
end

directory "/var/named/chroot/var/named/masters" do
  mode 0750
  owner "root"
  group "named"
  :create
end

template "named.conf.master" do
  path "/var/named/chroot/etc/named.conf"
  source "master.named.conf.erb"
  owner "root"
  group "named"
  mode 0640
end

template "/var/named/chroot/etc/masters.zones" do
  source "master.masters.zones.erb"
  owner "root"
  group "named"
  mode 0640
  notifies :restart, 'service[named]'
end

node['bind']['lookup'].each do |d|
  param = node['bind']['zone'].dup

  if d['zone']
    d['zone'].each do |key, val|
      param[key] = val
    end
  end

  template "/var/named/chroot/var/named/masters/" + d['domain'] + ".zone" do
    source "zonefile.erb"
    owner "named"
    group "named"
    mode 0644
    variables(
      :ttl        => param['ttl'],
      :serial     => param['serial'],
      :refresh    => param['refresh'],
      :retry      => param['retry'],
      :expire     => param['expire'],
      :minimum    => param['minimum'],
      :ns         => param['ns'],
      :mx         => param['mx'],
      :txt        => param['txt'],
      :a          => param['a'],
      :cname      => param['cname'],
      :nameserver => param['nameserver'],
      :email      => param['email']
    )
    notifies :reload, 'service[named]'
  end
end

node['bind']['rev_lookup'].each do |d|
  param = node['bind']['zone'].dup

  if d['zone']
    d['zone'].each do |key, val|
      param[key] = val
    end
  end

  template "/var/named/chroot/var/named/masters/" + d['domain'] + ".in-addr.arpa" do
    source "zonefile.rev.erb"
    owner "named"
    group "named"
    mode 0644
    variables(
      :ttl        => param['ttl'],
      :serial     => param['serial'],
      :refresh    => param['refresh'],
      :retry      => param['retry'],
      :expire     => param['expire'],
      :minimum    => param['minimum'],
      :ns         => param['ns'],
      :ptr        => param['ptr'],
      :nameserver => param['nameserver'],
      :email      => param['email']
    )
    notifies :reload, 'service[named]'
  end
end

service "named" do
  action [:enable, :start]
  supports :status => true, :restart => true, :reload => true
  subscribes :restart, resources(:template => ["named.conf.master", "/var/named/chroot/etc/masters.zones"])
end
