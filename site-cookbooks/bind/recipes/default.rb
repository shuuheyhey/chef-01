#
# Cookbook Name:: bind
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'bind::package'

template "/var/named/chroot/etc/named.conf" do
  source "named.conf.erb"
  owner "root"
  group "named"
  mode 0640
end

service "named" do
  action [:enable, :start]
  supports :status => true, :restart => true, :reload => true
  subscribes :restart, resources(:template => "/var/named/chroot/etc/named.conf")
end