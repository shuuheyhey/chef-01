default["bind"]["inner-nets"] = []
default["bind"]["slaves"]     = []
default[:bind][:rev_lookup]   = []
default[:bind][:lookup]       = []

default[:bind][:zone][:ttl]        = "3600"
default[:bind][:zone][:nameserver] = "ns.example.ne.jp."
default[:bind][:zone][:email]      = "hoge@hogehoge.jp"
default[:bind][:zone][:serial]     = "2010091001"
default[:bind][:zone][:refresh]    = "3600"
default[:bind][:zone][:retry]      = "1800"
default[:bind][:zone][:expire]     = "86400"
default[:bind][:zone][:minimum]    = "3600"

default[:bind][:zone][:ns]    = []
default[:bind][:zone][:mx]    = []
default[:bind][:zone][:txt]   = []
default[:bind][:zone][:a]     = []
default[:bind][:zone][:cname] = []
default[:bind][:zone][:ptr]   = []
