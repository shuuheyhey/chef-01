#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# package "mysql-libs" do
#   action :remove
#   only_if "test \"1\" = `rpm -qa | grep mysql-libs | wc -l`"
# end

case node['platform']
when "amazon"
  %w{mysql mysql-devel mysql-server}.each do |pkg|
    package pkg do
      action :install
    end
  end
when "centos", "redhat"
  %w{mysql mysql-devel mysql-server}.each do |pkg|
    package pkg do
      action :install
      options '--disablerepo=remi'
    end
  end
end

%w{/var/lib/mysql/data /var/lib/mysql/binlog /var/log/mysql}.each do |mysqldir|
  directory mysqldir do
    owner 'mysql'
    group 'mysql'
    mode  '0755'
    action :create
  end
end

%w{/var/log/mysql/error.log /var/log/mysql/slow-queries.log}.each do |mysqlfile|
  file mysqlfile do
    owner 'mysql'
    group 'mysql'
    mode  '0755'
    action :create
  end
end

template "/etc/my.cnf" do
  source "my.cnf.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :restart, 'service[mysqld]'
end

service "mysqld" do
  action [ :enable, :start]
  supports :status => true, :restart => true, :reload => true
  #subscribes :restart, resources(:template => "/etc/my.cnf")
end

script "Secure_Install" do
  interpreter 'bash'
  user "root"
  not_if "mysql -u root -p#{node['mysql']['root_password']} -e 'show databases'"
  code <<-EOL
    mysqladmin -u root password #{node['mysql']['root_password']}
    mysql -uroot -p#{node['mysql']['root_password']} -e "drop database test;delete from mysql.user where user='';"
  EOL
end
