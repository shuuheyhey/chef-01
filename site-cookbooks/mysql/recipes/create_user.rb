#
# Cookbook Name:: mysql
# Recipe:: create_user
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

schema = node['mysql']['schema']

node['mysql']['users'].each do |us|

  us['permit_host'].each do |hst|

    if us['role'] == 'admin'
      priv = 'ALL PRIVILEGES'
    elsif us['role'] == 'replication'
      priv = 'REPLICATION SLAVE'
    else
      priv = 'SELECT,INSERT,UPDATE,DELETE'
    end

    if us['schema']
      us_schema = us['schema']
    else
      us_schema = schema
    end

    us_schema.each do |sm|
      execute "create_user_" + us['name'] + "_" + hst do
        command "/usr/bin/mysql -u root -p#{node['mysql']['root_password']} -e \"GRANT #{priv} ON #{sm}.* TO \'#{us['name']}\'@\'#{hst}\' IDENTIFIED BY \'#{us['password']}\';\""
        action :run
        not_if "test \"1\" = `mysql -u root -p#{node['mysql']['root_password']} mysql -N -s -e \"select count(user) from user where user =\'#{us['name']}\' and host = \'#{hst}\';\"`"
      end
    end
  end
end

