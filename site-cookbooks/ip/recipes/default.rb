#
# Cookbook Name:: ip
# Recipe:: default
#
# Copyright 2015, Funaffect
#
# All rights reserved - Do Not Redistribute
#

node.ip.each do |ips|
  ifconfig ips["ip"] do
    target ips["ip"]
    device ips["target"]
    mask   ips["mask"]
  end
end