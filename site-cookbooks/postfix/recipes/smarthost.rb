#
# Cookbook Name:: postfix
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute

%w{postgrey patch python-pip}.each do |pkg|
  package pkg do
    action :install
  end
end

cookbook_file "/usr/sbin/targrey-0.31-postgrey-1.34.patch" do
  source "targrey-0.31-postgrey-1.34.patch"
  action :create_if_missing
end

cookbook_file "/tmp/pypolicyd-spf-1.1.2.tar.gz" do
  source "pypolicyd-spf-1.1.2.tar.gz"
  action :create_if_missing
end

bash "patch postgrey" do
  code <<-EOC
    cd /usr/sbin
    patch -p0 < targrey-0.31-postgrey-1.34.patch
    sed -i "s/, see http:.*\.html//g" /usr/sbin/postgrey
  EOC
  not_if "test \"0\" != `grep targrey /usr/sbin/postgrey | wc -l`"
end

bash "pypolicyd-spf install" do
  code <<-EOC
    cd /tmp
    tar zxf pypolicyd-spf-1.1.2.tar.gz
    cd pypolicyd-spf-1.1.2
    python setup.py install
  EOC
  not_if "test -f /usr/bin/policyd-spf"
end

%w{pyspf==2.0.7 pydns ipaddress}.each do |pippkg|
  python_pip pippkg
end

template "/etc/python-policyd-spf/policyd-spf.conf" do
  source "smarthost.policyd-spf.conf.erb"
  owner "root"
  group "root"
  mode 0644
end

node['postfix']['smarthost_files'].each do |sfiles|
  template "/etc/postfix/" + sfiles do
    source "smarthost." + sfiles + ".erb"
    owner "root"
    group "root"
    mode 0644
  end
end

node['postfix']['hash_files'].each do |hfiles|
  execute "hashfile_" + hfiles do
    command "/usr/sbin/postmap /etc/postfix/" + hfiles
    action :run
    notifies :restart, 'service[postfix]'
  end
end

template "/etc/postfix/main.cf" do
  source "smarthost.main.cf.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[postfix]'
end

template "/etc/postfix/master.cf" do
  source "smarthost.master.cf.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[postfix]'
end

template "/etc/sysconfig/postgrey" do
  source "smarthost.postgrey.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[postgrey]'
end

service "postgrey" do
  action [:enable, :start]
  supports :status => true, :restart => true, :reload => true
  subscribes :restart, resources(:template => "/etc/sysconfig/postgrey")
end
