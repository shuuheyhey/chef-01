#
# Cookbook Name:: postfix
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

package "postfix" do
  action :install
end

execute "IPv4 Only use" do
  command <<-EOC
    sed -i 's/^inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
  EOC
  action :run
  not_if "test \"1\" = `grep 'inet_protocols = ipv4' /etc/postfix/main.cf | wc -l`"
end

service "postfix" do
  action [ :enable, :start]
  supports :status => true, :restart => true, :reload => true
  #subscribes :reload, resources(:template => "/etc/postfix/main.cf")
end