#
# Cookbook Name:: zabbix server
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

# zabbix-agent

case node['platform']
when "centos", "redhat"
  version = '$releasever'
when "amazon"
  version = '6'
end

%w{zabbix-server zabbix-web zabbix-web-mysql ipa-pgothic-fonts}.each do |pkg|
  package pkg do
    action :install
  end
end

execute "Japanese Font install" do
  command <<-EOC
    alternatives --install /usr/share/zabbix/fonts/graphfont.ttf zabbix-web-font /usr/share/fonts/ipa-pgothic/ipagp.ttf 1000
   EOC
  action :run
  not_if "test 1 -lt `alternatives --display zabbix-web-font | grep 'ipa-pgothic' | wc -l`"
end

execute "create zabbix table" do
  command <<-EOC
    cd /usr/share/doc/zabbix-server-mysql-*/create
    /usr/bin/mysql -u#{node['zabbix_server']['dbuser']} -p#{node['zabbix_server']['dbpassword']} #{node['zabbix_server']['dbname']} < schema.sql
    /usr/bin/mysql -u#{node['zabbix_server']['dbuser']} -p#{node['zabbix_server']['dbpassword']} #{node['zabbix_server']['dbname']} < images.sql
    /usr/bin/mysql -u#{node['zabbix_server']['dbuser']} -p#{node['zabbix_server']['dbpassword']} #{node['zabbix_server']['dbname']} < data.sql
  EOC
  action :run
  not_if "test \"1\" = `/usr/bin/mysql -u#{node['zabbix_server']['dbuser']} -p#{node['zabbix_server']['dbpassword']} #{node['zabbix_server']['dbname']} -N -s -e \"select count(userid) from users where userid =\'1\';\"`"
end

template "/etc/zabbix/zabbix_server.conf" do
  group "root"
  owner "root"
  mode 0640
  source "zabbix_server.conf.erb"
  notifies :restart, 'service[zabbix-agent]'
end

service "zabbix-server" do
  action [ :enable, :start]
  supports :status => true, :restart => true
end
