#
# Cookbook Name:: iptables
# Recipe:: default
#
# Copyright 2014, Funaffect Inc.
#
# All rights reserved - Do Not Redistribute
#

package "iptables" do
  action :install
end

template "/etc/sysconfig/iptables" do
  source "iptables.erb"
  owner "root"
  group "root"
  mode 0644
  variables(
    :list => node['iptables']['rule']
  )
  notifies :reload, 'service[iptables]'
end

service "iptables" do
  action [ :enable, :start]
  supports :status => true, :restart => true
  subscribes :restart, resources(:template => "/etc/sysconfig/iptables")
end
