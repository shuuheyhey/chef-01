name             'httpd'
maintainer       'Funaffect Inc.'
maintainer_email 'm.oobayashi@funaffect.jp'
license          'All rights reserved'
description      'Installs/Configures httpd'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.4'

depends          'yum-epel'
depends          'yum-remi'
