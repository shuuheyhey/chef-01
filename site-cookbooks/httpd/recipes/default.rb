#
# Cookbook Name:: httpd
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

#p node['recipes'].grep(/^httpd::php7/).count
case node['platform']
when "amazon"
  if node['recipes'].grep(/^httpd::php7/).count > 0
    include_recipe "httpd::httpd24"
  else
    include_recipe "httpd::httpd22"
  end
when "centos", "redhat"
  if node['platform_version'].to_i >= 7
    include_recipe "httpd::httpd24"
  else
    include_recipe "httpd::httpd22"
  end
end
 
