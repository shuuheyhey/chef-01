#
# Cookbook Name:: httpd
# Recipe:: mt
#
# Copyright 2015, Funaffect
#
# All rights reserved - Do Not Redistribute
#

%w{perl-Class-DBI-mysql perl-Archive-Zip perl-Digest-SHA3 perl-Digest-SHA1 perl-Archive-Tar perl-GD perl-Authen-SASL ImageMagick-perl}.each do |pkg|
  package pkg do
    action :install
  end
end

 