#
# Cookbook Name:: httpd
# Recipe:: php
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

case node['platform']
when "amazon"
  %w{php55 php55-devel php55-gd php55-mbstring php-pear php55-bcmath php55-mysqlnd php55-pdo php55-xml php55-mcrypt}.each do |pkg|
    package pkg do
      action :install
    end
  end
when "centos", "redhat"
  # Need Remi Repository
  %w{php php-devel php-gd php-mbstring php-pear php-bcmath php-mysqlnd php-pdo php-xml php-mcrypt}.each do |pkg|
    package pkg do
      action :install
      options "--enablerepo=remi-php55"
    end
  end
end


template "/etc/php.ini" do
  source "php55.ini.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end