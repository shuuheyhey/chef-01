#
# Cookbook Name:: httpd
# Recipe:: php
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

case node['platform']
when "amazon"
  %w{php56 php56-devel php56-gd php56-mbstring php-pear php56-bcmath php56-mysqlnd php56-pdo php56-xml php56-mcrypt}.each do |pkg|
    package pkg do
      action :install
    end
  end
when "centos", "redhat"
  include_recipe "yum-epel"
  include_recipe "yum-remi"

  # Need Remi Repository
  %w{php php-devel php-gd php-mbstring php-pear php-bcmath php-mysqlnd php-mcrypt php-pecl-zip}.each do |pkg|
    package pkg do
      action :install
      options "--enablerepo=remi-php56"
    end
  end
end


template "/etc/php.ini" do
  source "php55.ini.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end
