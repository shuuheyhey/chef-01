default[:virtualhosts][:rotate] = "90"

default[:virtualhosts][:namebase_ips] = []

default[:virtualhost][:param][:template]       = "default"
default[:virtualhost][:param][:email]          = "hoge@hogehoge.jp"
default[:virtualhost][:param][:ipaddr]         = "*"
default[:virtualhost][:param][:port]           = "80"
default[:virtualhost][:param][:port_ssl]       = "443"
default[:virtualhost][:param][:fqdn]           = "example.hogehoge.jp"
default[:virtualhost][:param][:ftp_id]         = "exampleftpuser"
default[:virtualhost][:param][:ftp_password]   = "exampleftpuser"
default[:virtualhost][:param][:basic_id]       = "examplebasic"
default[:virtualhost][:param][:basic_password] = "examplebasic"
default[:virtualhost][:param][:ssl]            = "disable"
default[:virtualhost][:param][:is_ssl]         = false
default[:virtualhost][:param][:is_use_auth]    = false
