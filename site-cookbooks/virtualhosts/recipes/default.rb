#
# Cookbook Name:: virtualhosts
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

ips      = node.virtualhosts.namebase_ips
vh_list  = node['virtualhosts']['vh_list']
ssl_path = "/etc/httpd/conf/ssl."

if ips.empty? == false
  template "/etc/httpd/conf.d/namebase.conf" do
    owner "root"
    group "root"
    mode 0644
  end
end

# create default directory
directory '/home/webroot' do
  mode 0775
  action :create
end

%w{key csr cha cer}.each do |sshdir|
  directory ssl_path + sshdir do
    mode 0775
    action :create
  end
end

template "/etc/logrotate.d/httpd" do
  owner "root"
  group "root"
  mode 0644
end

vh_list.each do |v|

  param = node['virtualhost']['param'].dup

  if v['param']
    v['param'].each do |key, val|
      param[key] = val
    end
  end

  if param['ftp_group'].nil? == true
    ftpgroup = param['ftp_id']
  else
    ftpgroup = param['ftp_group']
  end

  group ftpgroup do
    action :create
  end

  user param['ftp_id'] do
    home '/home/webroot/' + param['fqdn']
    shell '/bin/bash'
    gid ftpgroup
    password param['ftp_password']
    supports :manage_home => true
    action :create
  end

  user_id   = param['ftp_id']
  user_pass = param['ftp_password']

  bash "user auth " + v['name'] do
    code <<-EOH
      echo #{user_id}:#{user_pass} | /usr/sbin/chpasswd 
    EOH
  end

  directory '/etc/httpd/htpasswd for ' + v['name'] do
    path '/etc/httpd/htpasswd'
    mode 0755
    :create
  end

  file '/etc/httpd/htpasswd/.htpasswd-' + v['name'] do
    owner "root"
    group "root"
    mode "0755"
    action :create_if_missing
  end

  basic_host = v['name']
  basic_auth = param['basic_id']
  basic_pass = param['basic_password']

  bash "basic auth " + v['name'] do
    code <<-EOH
      /usr/bin/htpasswd -b -m /etc/httpd/htpasswd/.htpasswd-#{basic_host} system "funa_sys0)8'" > /dev/null 2>&1
      /usr/bin/htpasswd -b -m /etc/httpd/htpasswd/.htpasswd-#{basic_host} #{basic_auth} "#{basic_pass}" > /dev/null 2>&1
    EOH
  end

  directory '/home/webroot/' + param['fqdn'] do
    mode 0775
    action :create
  end

  %w{htdocs logs}.each do |dir|
    directory '/home/webroot/' + param['fqdn'] + '/' + dir do
      owner param['ftp_id']
      group ftpgroup
      mode 0775
      action :create
    end
  end

  if param['ipaddr']
    ipaddr = param['ipaddr']
  else
    ipaddr = '*'
  end

  template "/etc/httpd/conf.d/vhost." + v['name'] + ".conf" do
    source param['template'] + ".conf.erb"
    owner "root"
    group "root"
    mode 0644
    variables(
      :sitename     => v['name'],
      :ipaddr       => ipaddr,
      :port         => param['port'],
      :email        => param['email'],
      :fqdn         => param['fqdn'],
      :alias        => param['alias'] ? param['alias'] : [],
      :env          => param['env'] ? param['env'] : [],
      :documentroot => param['documentroot'] ? param['documentroot'] : '/home/webroot/' + param['fqdn'],
      :base_dir     => '/home/webroot/' + param['fqdn']
    )
    notifies :reload, 'service[httpd]'
  end

  if param['ssl'] == "enable"
    vh = data_bag_item('virtualhosts', v['name'])

    %w{key cha cer}.each do |sshdata|
      file ssl_path + sshdata + "/" + param['fqdn'] + '.' + sshdata do
        content vh[sshdata]
        owner 'root'
        group 'root'
        mode '644'
      end
    end

    template "/etc/httpd/conf.d/vhost.ssl." + v['name'] + ".conf" do
      source param['template'] + ".ssl.conf.erb"
      owner "root"
      group "root"
      mode 0644
      variables(
        :sitename     => v['name'],
        :ipaddr       => ipaddr,
        :email        => param['email'],
        :fqdn         => param['fqdn'],
        :port_ssl     => param['port_ssl'],
        :ssl_path     => ssl_path,
        :alias        => param['alias'] ? param['alias'] : [],
        :env          => param['env'] ? param['env'] : [],
        :documentroot => param['documentroot'] ? param['documentroot'] : '/home/webroot/' + param['fqdn'],
        :base_dir     => '/home/webroot/' + param['fqdn']
      )
      notifies :reload, 'service[httpd]'
    end
  end

end
