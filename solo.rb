# solo.rb
base = File.expand_path('..', __FILE__)

file_cache_path "/tmp/chef-solo"
cookbook_path [base + "/cookbooks", base + "/site-cookbooks"]
data_bag_path [base + "/data_bags"]
