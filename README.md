README
======

* Download

```
  curl -L https://www.chef.io/chef/install.sh | bash
```

* Setting

```
  yum update -y
  yum install git -y
  git clone https://bitbucket.org/shuuheyhey/chef-01.git
```


* Chenge Directory

```
  cd chef-01
```

* Knife
  レシピを追加する場合

```
knife cookbook site install yum -o cookbooks
knife cookbook site install yum-epel -o cookbooks
knife cookbook site install yum-remi -o cookbooks
```

* Run

```
  chef-solo -c solo.rb -j ./localhost.json
```
